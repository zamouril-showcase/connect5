module zamouriltomas.connect5 {
    requires transitive javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    opens zamouriltomas.connect5 to javafx.fxml;
    exports zamouriltomas.connect5;
}
