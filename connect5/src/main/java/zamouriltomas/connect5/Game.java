package zamouriltomas.connect5;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import zamouriltomas.connect5.Plugin.Player;

public class Game {

    public static int rows = 16; //rows on the game board
    public static int columns = 16; //columns on the game board
    static char[][] board = new char[rows][columns]; //2d char array of the board
    static char currentPlayer = 'X'; //X for player 1, O for player 2
    StringProperty victoryStatus = new SimpleStringProperty("Nobody has won"); //value for victoryLabel on game scene
    StringProperty nextToPlay = new SimpleStringProperty("Next to play is: " + Character.toString(currentPlayer));
    static String gameMode = "pvp";
    static Player players[] = new Player[2];


    
    void buildGame(GridPane buttonGrid) {

        //loads bot classes from jar




        //builds button grid
        for (int x = 0; x < columns; x++){
            for (int y = 0; y < rows; y++){

                //resets board to default value
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        board[i][j] = '\0';
                    }
                }
                //creates button
                Button tile = new Button();
                tile.setPrefSize(32, 32);
                tile.setStyle("-fx-opacity: 1.0;");
                
                //required for lambda function to work
                int col = x;
                int row = y;
    
                //updates tile on gameboard when clicked
                tile.setOnAction(event -> updateTile(buttonGrid, tile, row, col));

                buttonGrid.add(tile, x, y);

                victoryStatus.setValue("Nobody has won");
                currentPlayer = 'X';
            }
        }
    }

    private void updateTile(GridPane buttonGrid, Button tile, int row, int col) {

        //assign button to the player
        tile.setText(Character.toString(currentPlayer));

        //change button color to player's color
        if (currentPlayer == 'X'){
            tile.setStyle("-fx-text-fill: blue; -fx-opacity: 1.0;");
        } else {
            tile.setStyle("-fx-text-fill: red; -fx-opacity: 1.0;");
        }

        tile.setDisable(true);

        //assign value to the char array
        board[row][col] = currentPlayer;

        //check win state
        if(CheckWin.isWin(row, col)){

            disableTiles(buttonGrid);
            victoryStatus.setValue("player " + Character.toString(currentPlayer) + " has won");
        } else  

        //switch to next player
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
            if(!gameMode.equals("pvp")){
                players[1].botMove(buttonGrid);
                //RandomBOTtest.botMove(buttonGrid);
                
            }
        } else {
            currentPlayer = 'X';
            if(gameMode.equals("bvb")){
                players[0].botMove(buttonGrid);
                //RandomBOTtest.botMove(buttonGrid);
            }
        }
 
        nextToPlay.setValue("Next to play is: " + Character.toString(currentPlayer));
        
    }

    void gameRestart(GridPane buttonGrid) {
         //resets board to default value
         for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                board[i][j] = '\0';
            }
        }
        //rebuilds tiles, cleaner than changing text of each button
        buildGame(buttonGrid);
        victoryStatus.setValue("Nobody has won");
        Game.currentPlayer = 'X';
    }

    //disables tiles after player won
    private void disableTiles(GridPane buttonGrid) {
        for(Node node : buttonGrid.getChildren()){
            Button button = (Button) node;
            button.setDisable(true);
        }
    }


}
