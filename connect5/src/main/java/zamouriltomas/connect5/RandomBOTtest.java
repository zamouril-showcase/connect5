package zamouriltomas.connect5;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import zamouriltomas.connect5.Plugin.Player;

public class RandomBOTtest implements Player {

    public void botMove (GridPane buttonGrid) {
        //create random coordinates
        int x = (int) (Math.random() * Game.columns);
        int y = (int) (Math.random() * Game.rows);

        //if you find an empty button click it, else try different coordinates
        for(Node node : buttonGrid.getChildren()){
            if (GridPane.getColumnIndex(node) == x && GridPane.getRowIndex(node) == y){
                Button b = (Button) node;
                if (b.isDisabled()){
                    botMove(buttonGrid);
                } else {
                b.fire();
                break;
                }
            }
        }
    }
    public void testMessage(){
        System.out.println("funguju");
    }
}
