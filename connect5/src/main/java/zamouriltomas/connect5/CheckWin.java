package zamouriltomas.connect5;

public class CheckWin {


    static boolean isWin(int row, int col) {
        int inRow = 0;

        //check row of last updated tile
        for(int i = 0; i < Game.columns; i++){
            if(Game.board[row][i] == Game.currentPlayer){
                inRow++;
                if(inRow == 5){
                    return true;
                }
            } else {
                inRow = 0;
            }
        }

        //check column of last updated tile
        inRow = 0;
        for(int i = 0; i < Game.rows; i++){
            if(Game.board[i][col] == Game.currentPlayer){
                inRow++;
                if(inRow == 5){
                    return true;
                }
            } else {
                inRow = 0;
            }
        }

        //check diagonal from top left to bottom right of last updated tile
        inRow = 0;
        for (int r = row - 4, c = col - 4; r <= row + 4 && c <= col + 4; r++, c++) {
            if(r < 0 || c < 0 || r >= Game.rows || c >= Game.columns){
                continue;
            } else if (Game.board[r][c] == Game.currentPlayer) {
                inRow++;
                if (inRow == 5) {
                    return true;
                }
            } else {
                inRow = 0;
            }
        }

        //check diagonal from bottom left to top right of last updated tile
        inRow = 0;
        for (int r = row + 4, c = col - 4; r >= row - 4 && c <= col + 4; r--, c++) {
            if(r < 0 || c < 0 || r >= Game.rows || c >= Game.columns){
                continue;
            }else if (Game.board[r][c] == Game.currentPlayer) {
                inRow++;
                if (inRow == 5) {
                    return true;
                }
            } else {
                inRow = 0;
            }
        }

        return false;
    }
}
