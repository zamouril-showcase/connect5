package zamouriltomas.connect5;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import zamouriltomas.connect5.Plugin.Player;

public class SetupController {

    private List<File> selectedFiles; //saves all the selected files from FileChooser to know which class to load

    @FXML
    private RadioButton pvp, pvb, bvb;
    @FXML
    private Button startButton, upload, backButton;
    @FXML
    private ListView <String> listBots;
    @FXML
    private ChoiceBox <String> pvbSelect, bvbSelect1, bvbSelect2;

    @FXML
    private void initialize(){

        //player has to choose before start
        startButton.setDisable(true);

        //creates arraylist of choiceboxes
        ArrayList<ChoiceBox<String>> cBoxes = new ArrayList<>();
        cBoxes.add(pvbSelect);
        cBoxes.add(bvbSelect1);
        cBoxes.add(bvbSelect2);

        
        for (ChoiceBox<String> cb : cBoxes){

            //binds choiceboxes to ListView of all bots
            cb.itemsProperty().bind(listBots.itemsProperty());

            //when bot is selected, load the class
            cb.getSelectionModel().selectedItemProperty().addListener((Observable, oldValue, newValue) -> {

                //when matching bot vs bot one bot has to be X
                if(cb.getId().equals("bvbSelect1")){

                    Game.players[0] = Player.loadFromFile(getFile(cb));
                    Game.players[0].testMessage();

                } else {

                    Game.players[1] = Player.loadFromFile(getFile(cb));
                    Game.players[1].testMessage();
                }
            });
        }
    }

    private File getFile(ChoiceBox<String> cb) {

        //goes through each of the files selected in FileChooser, if the name of the file matches the selected bot, return the file to classload
        for (File f : selectedFiles){
            if (f.getName().replaceFirst("[.][^.]+$", "").equals(cb.getValue())){
                return f;
            } 
        }
        return null; 
    }

    @FXML
    private void playerVsPlayer(){
        Game.gameMode = "pvp";
        startButton.setDisable(false);
    }

    @FXML
    private void playerVsBot(){
        Game.gameMode = "pvb";
        startButton.setDisable(false);
    }

    @FXML
    private void BotVsBot(){
        Game.gameMode = "bvb";
        startButton.setDisable(false);
    }

    @FXML
    private void switchToGame() throws IOException {
        App.setRoot("game");
    }

    @FXML
    private void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    @FXML
    private void selectFile(){

        //select jar file of the bot
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("JAR Files", "*.jar"));
        selectedFiles = fc.showOpenMultipleDialog(null);

        //add bot to the selection list
        if (selectedFiles != null){
            for (File f : selectedFiles){
                listBots.getItems().add(f.getName().replaceFirst("[.][^.]+$", ""));
            }
        } else {
            System.out.println("invalid file");
        }
    }
    
}
