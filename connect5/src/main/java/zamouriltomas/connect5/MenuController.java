package zamouriltomas.connect5;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class MenuController {

    @FXML
    private Pane root = new Pane();

    @FXML
    private void switchToSetup() throws IOException {
        App.setRoot("setup");
    }

    @FXML
    private void initialize(){
        keepAspectRatio();
    }

    @FXML
    private void exitGame(){
        Platform.exit();
    }

    private void keepAspectRatio() {
        //when root is assigned to scene
        //currently doesnt work??
        root.sceneProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null){
                //set root to keep aspect ratio, while resizing when stage is resized
                root.prefWidthProperty().bind(root.getScene().widthProperty());
                root.prefHeightProperty().bind(root.getScene().widthProperty().multiply(720 / 1280));
            }
        });
    }

}
