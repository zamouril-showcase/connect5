package zamouriltomas.connect5;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class App extends Application {

    static Scene currentScene;

    public static void main(String[] args) {
        //TODO:
        //fix gui - keepaspectratio in menucontroller doesnt work
        //create css files for fxml
        //add hidable buttons for bvb mode
        //smarter bots

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        //sets first scene to be menu
        currentScene = new Scene(loadFXML("menu"), 1280, 720);
        
        //show menu stage
        primaryStage.setScene(currentScene);
        primaryStage.setTitle("Connect 5");
        primaryStage.show();

    }

    //changes current scene to scene in argument
    static void setRoot(String fxml) throws IOException {
        currentScene.setRoot(loadFXML(fxml));
    }

    //loads FXML of scene in argument
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

}