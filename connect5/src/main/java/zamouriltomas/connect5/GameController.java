package zamouriltomas.connect5;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class GameController {

    //creates instance of method Game so that there is no need for static methods
    private Game gameInstance = new Game();

    @FXML
    private GridPane buttonGrid = new GridPane();

    @FXML
    private Label victoryLabel = new Label();

    @FXML
    private Label nextToPlayLabel = new Label();

    @FXML
    private void initialize(){
        
        gameInstance.buildGame(buttonGrid);

        //bind labels to correct values
        victoryLabel.textProperty().bind(gameInstance.victoryStatus);
        nextToPlayLabel.textProperty().bind(gameInstance.nextToPlay);
    }

    @FXML
    private void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    @FXML
    private void restart() throws IOException{
        App.setRoot("game");;
    }
    
}