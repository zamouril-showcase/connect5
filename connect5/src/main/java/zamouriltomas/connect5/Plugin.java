package zamouriltomas.connect5;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarFile;

import javafx.scene.layout.GridPane;
//import zamouriltomas.connect5.Game;


//dynamically loaded bot needs to implement Player interface, only botMove method is required
public class Plugin {

    //declares methods for bot player
    public interface Player {
        public void botMove(GridPane buttonGrid); //change to: public void botMove(Game game);
        public void testMessage(); 

        public static Player loadFromFile(File f) {
            try {
                // get class path from manifest
                JarFile jarf = new JarFile(f);
                String classPath = jarf.getManifest().getMainAttributes().getValue("Main-Class");
                jarf.close();

                // get URL from file
                URL myUrl = f.toURI().toURL();

                // load class
                URLClassLoader loader = new URLClassLoader(new URL[] { myUrl });
                Class<?> loadedClass = loader.loadClass(classPath);
                loader.close();

                return (Player) loadedClass.getDeclaredConstructor().newInstance();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }
    }
}
